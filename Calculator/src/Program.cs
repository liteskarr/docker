using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


public static class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.Services.AddControllers();
        builder.Services.AddSwaggerGenNewtonsoftSupport();
        builder.Services.AddControllers().AddNewtonsoftJson(c =>
        {
            c.SerializerSettings.Converters.Add(new StringEnumConverter());
        });
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        var app = builder.Build();
        
        app.UseSwagger();
        app.UseSwaggerUI();

        app.UseAuthorization();

        app.MapControllers();

        app.Run();
    }
}