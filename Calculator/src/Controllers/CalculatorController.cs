﻿using Calculator.Models;
using Microsoft.AspNetCore.Mvc;

namespace Calculator;

/// <summary>
/// Main Calculator Controller.
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class CalculateController : Controller
{
    /// <summary>
    /// Returns result of operation for X and Y params.
    /// </summary>
    /// <param name="op">Operation</param>
    /// <param name="x">First argument</param>
    /// <param name="y">Second argument</param>
    /// <returns>Returns result of operation</returns>
    /// <exception cref="ArgumentException">Throws when an invalid operation is passed</exception>
    private double ProcessOperation(CalculatorOperation op, double x, double y)
    {
        return op switch
        {
            CalculatorOperation.Plus => x + y,
            CalculatorOperation.Minus => x - y,
            CalculatorOperation.Mult => x * y,
            CalculatorOperation.Div => x / y,
            _ => throw new ArgumentException()
        };
    }
    
    /// <summary>
    /// Post handler.
    /// </summary>
    /// <param name="request">Body of request</param>
    /// <returns>Returns result of handling</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public ActionResult Post(CalculateRequest request)
    {
        try
        {
            var result = ProcessOperation(request.Operation, request.X, request.Y);
            if (!double.IsFinite(result)) throw new ArithmeticException("Arithmetic error!");
            return Ok(new CalculatorResponse(result));
        }
        catch (Exception e)
        {
            return BadRequest(new CalculatorResponse(0));
        }
    }
}