﻿namespace Calculator.Models;

public record CalculatorResponse(double Result);