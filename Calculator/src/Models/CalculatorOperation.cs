﻿namespace Calculator.Models;

public enum CalculatorOperation
{
    Plus,
    Minus,
    Mult,
    Div,
}