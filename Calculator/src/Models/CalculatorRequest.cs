﻿namespace Calculator.Models;

public record CalculateRequest(CalculatorOperation Operation, double X, double Y);